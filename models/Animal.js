const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// here are the collections / or the table in database
const AnimalSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  isEndangered: {
    type: Boolean,
    default: true
  },
  dateOfEntry: {
    type: Date,
    default: Date.now()
  }
});
module.exports = Item = mongoose.model('animal', AnimalSchema);