const express = require('express');
const mongoose = require('mongoose');
const config = require('config');

const app = express();

const db = config.get('mongoURI'); 
mongoose
  .connect(db, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false })
  .then(() => console.log('MongoDB Connected...'))
  .catch(err => console.log(err)); 

  // Inserting to Database
/*
const Animal = require('./models/Animal');
const newAnimal = new Animal({
  name: 'Pepi',
  isEndangered: false
})
newAnimal
  .save()
  .then(item => console.log(item))
  .catch(err => console.log(err));
  */  

    // Find all items 
/*
 const Animal = require('./models/Animal');
 Animal.find()
  .sort({ date: -1 })
  .then(items => console.log(items));

*/ 
 

    //update item 
/*
const Animal = require('./models/Animal'); 

Animal
  .findOneAndUpdate(
    { _id: '5d832fe01373981d50f51bef' },
    { isEndangered: false }
  )
  .then(item => console.log(item));

  */ 
 //delete items 
 /* 
 const Animal = require('./models/Animal'); 
 Animal
  .findOneAndDelete(
    { _id: '5d832fe01373981d50f51bef' },
    { isEndangered: false }
  )
  .then(console.log('Item deleted'));

  */